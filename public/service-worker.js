'use strict';
importScripts('https://www.gstatic.com/firebasejs/4.1.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.1.2/firebase-messaging.js');

var config = {
    apiKey: "AIzaSyBEltCJ-kdjYWYhHLoq0Bpc53EH9QsZQAw",
    authDomain: "enotify-ad754.firebaseapp.com",
    databaseURL: "https://enotify-ad754.firebaseio.com",
    projectId: "enotify-ad754",
    storageBucket: "enotify-ad754.appspot.com",
    messagingSenderId: "399588624045"
};

function isFunction(obj) {
    return obj && {}.toString.call(obj) === '[object Function]';
}

function runFunctionString(funcStr) {
    if (typeof funcStr != "undefined") {
        if (funcStr.trim().length > 0) {
            var func = new Function(funcStr);
            if (isFunction(func)) {
                func();
            }
        }
    }
}

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    const title = payload.data.title;
    const opts = {
        body: payload.data.body,
        icon: payload.data.icon,
        image: payload.data.image,
        requireInteraction: payload.data.requireInteraction,
        tag: 'require-interaction',
        data: {
            link: payload.data.link
        }
    }
    return self.registration.showNotification(title, opts);
});

self.addEventListener('message', function (event) {
    self.client = event.source;
});

self.onnotificationclose = function (event) {
    runFunctionString(event.notification.data.onClose);

    /* Tell Push to execute close callback */
    self.client.postMessage(
        JSON.stringify({
            id: event.notification.data.id,
            action: 'close'
        })
    );
};

self.onnotificationclick = function (event) {
    var link;
    var bypass = true;

    if (event.action === 'purchase') {
        event.notification.close();
        return clients.openWindow(event.notification.data.purchase);
    }
    if (event.action === 'view') {
        event.notification.close();
        return clients.openWindow(event.notification.data.link);
    }

    if (
        typeof event.notification.data.link !== 'undefined' &&
        event.notification.data.link !== null
    ) {
        link = event.notification.data.link;

        event.notification.close();

        /* This looks to see if the current is already open and focuses if it is */
        event.waitUntil(
            clients
            .matchAll({
                type: 'window'
            })
            .then(function (clientList) {
                var client, full_url;

                for (var i = 0; i < clientList.length; i++) {
                    client = clientList[i];
                    full_url = link;

                    if (
                        full_url[full_url.length - 1] !== '/' &&
                        client.url[client.url.length - 1] === '/'
                    ) {
                        full_url += '/';
                    }

                    if (client.url === full_url && 'focus' in client) {
                        return client.focus();
                    }
                }

                if (clients.openWindow) {
                    return clients.openWindow(link);
                }
            })
        );
    }

    runFunctionString(event.notification.data.onClick);
};