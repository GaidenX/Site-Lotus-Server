var projectsConfig;
// ---- Index ----
$(document).ready(function () {
	// Carregamento do Json e criação da home
	$.get("/homeconfig", function (data) {
		projectsConfig = data;
	}).done(function () {
		var gameHtml = '<div class="game-area" data-id="0">' +
			'<div id="left-sidebar" class="col-lg-6 col-md-6">' +
			'<h2 class="game-title">Between Squares</h2>' +
			'<p class="game-description"></p>' +
			'<span class="specifyTitle">Especificações</span>' +
			'<ul><li class="specifyRelease"><strong>Lançamento: </strong><span></span></li>' +
			'<li class="specifyGender"><strong>Genero: </strong><span></span></li>' +
			'<li class="specifyLanguage"><strong>Idioma: </strong><span></span></li>' +
			'<li class="specifyMultiplayer"><strong>Multiplayer: </strong><span></span></li>' +
			'<li class="specifyPlatform"><strong>Plataforma: </strong><span></span></li></ul>' +
			'</div>' +
			'<div id="right-sidebar" class="col-lg-6 col-md-6">' +
			'<div class="rightMenu"><ul class="showSlider"></ul></div>' +
			'</div>' +
			'<div class="col-lg-12 col-md-12">' +
			'<div class="game-play">' +
			'<p>Gostou do jogo? Aproveite e jogue agora.</p>' +
			'<a class="open-game" href="#" target="_blank"></a>' +
			'</div></div>' +
			'</div>';

		// 1 Card Infos
		for (var key in projectsConfig.projects) {
			$('.game-session #projects-container').append(gameHtml);
			$('.game-area:eq(' + key + ')').attr('id', projectsConfig.projects[key].id);
			// main banner
			$('.showSlider:eq(' + key + ')').attr('id', 'showSlider' + key);
			$('.showSlider:eq(' + key + ')').append('<li data-id="' + key + '"><a href="' + projectsConfig.projects[key].mainBanner + '" data-lightbox="' + projectsConfig.projects[key].id + '"><div class="game-box-img"><img src="' + projectsConfig.projects[key].mainBanner + '" /></div></a></li>'); //<p>Jogue agora</p>
			// Gallery
			for (var items in projectsConfig.projects[key].gallery)
				$('.showSlider:eq(' + key + ')').append('<li><a href="' + projectsConfig.projects[key].gallery[items].url + '" data-lightbox="' + projectsConfig.projects[key].id + '" data-title="' + projectsConfig.projects[key].gallery[items].title + '"><div class="game-box-img"><img src="' + projectsConfig.projects[key].gallery[items].url + '" title="' + projectsConfig.projects[key].gallery[items].title + '"></div></a></li>')
			// Button
			$('.open-game:eq(' + key + ')').text(projectsConfig.projects[key].buttonText);

			// Card Infos
			// $('#specifyAge:eq('+key+') span').text(projectsConfig.projects[0].infos.specifyAge);

			$('.game-title:eq(' + key + ')').text(projectsConfig.projects[key].name);
			$('.game-description:eq(' + key + ')').html(projectsConfig.projects[key].infos.descriptionText.replace(/\n/g, '<br>'));
			$('.specifyRelease:eq(' + key + ') span').text(projectsConfig.projects[key].infos.specifyRelease);
			$('.specifyGender:eq(' + key + ') span').text(projectsConfig.projects[key].infos.specifyGender);
			$('.specifyLanguage:eq(' + key + ') span').text(projectsConfig.projects[key].infos.specifyLanguage);
			$('.specifyMultiplayer:eq(' + key + ') span').text(projectsConfig.projects[key].infos.specifyMultiplayer);
			$('.specifyPlatform:eq(' + key + ') span').text(projectsConfig.projects[key].infos.specifyPlatform);

			$('.open-game:eq(' + key + ')').attr('href', projectsConfig.projects[key].url);
		}

		let checkView = function () {
			for (var key in projectsConfig.projects) {
				if (checkViewPort('#' + projectsConfig.projects[key].id + '')) {
					$('#' + projectsConfig.projects[key].id + '').addClass('in-screen');
				} else {
					$('#' + projectsConfig.projects[key].id + '').removeClass('in-screen');
				}
			}
		}

		$(function () {
			checkView();
		});

		$(document).scroll(function () {
			checkView();
		});

		//Configuracoes das imagens lightbox 
		lightbox.option({
			'resizeDuration': 200,
			'maxHeight': 500,
			'wrapAround': true,
			'fadeDuration': 500,
			'alwaysShowNavOnTouchDevices': true,
			'disableScrolling': false,
			'position': 'fixed'
		});

		for (var key in projectsConfig.projects) {
			var slider = tns({
				"container": '#showSlider' + key,
				"items": 1,
				"nav": false,
				"slideBy": "page",
				"speed": 400,
				"loop": true,
				"autoplay": true,
				"autoplayHoverPause": true,
				"autoplayTimeout": 8000,
				"controls": true,
				"mouseDrag": true,
				"controlsText": ['', '']
			});
		}
	});
});

// //----- contact -----
// $("contact.html").ready(function () {
// 	var disqus_config = function () {
// 		this.page.url = PAGE_URL; //pode por o link para diminuir o risco de copia
// 		this.page.identifier = PAGE_IDENTIFIER; // pode por a variavel de identificacao para diminuir o risco de copia
// 	};

// 	(function () { // DON'T EDIT BELOW THIS LINE
// 		var d = document,
// 			s = d.createElement('script');

// 		s.src = '//lotusgamesdev.disqus.com/embed.js';

// 		s.setAttribute('data-timestamp', +new Date());
// 		(d.head || d.body).appendChild(s);
// 	})();
// });

// function checkEmail() {
// 	// isto é para acontecer somente quando for computadores
// 	if (($(window).width()) >= 768)
// 		$('#formEmail').attr('action', 'mailto:lotusgamesdev@gmail.com?subject=' + $("#Mysubject").val() +
// 			'&body=' + $("#Mymessage").val() + '');
// 	// isto é para acontecer somente quando for mobiles
// 	else
// 		sendEmail();
// }

// function sendEmail() {
// 	$.ajax({
// 		traditional: true,
// 		type: "POST",
// 		dataType: "json",
// 		contentType: "application/json; charset=utf-8",
// 		url: "/sendEmail",
// 		data: JSON.stringify({
// 			"email": $("#userEmail").val(),
// 			"subject": $("#Mysubject").val(),
// 			"message": $("#Mymessage").val()
// 		}),
// 		success: function (response) {
// 			console.log(response);
// 		}
// 	});
// }