![](public/image/innix.png)

# Sinopse

Projeto do site da Empresa voltada para Jogos e Aplicativos mobiles principalmente, porém desenvolve para web e computadores em conjunto. Em desenvolvimento através da metodologia de mobile first.


# Link para teste mobile: 

Site hospedado em servidores da Heroku App

[Url para acesso, clique aqui!](https://innix.herokuapp.com/) 

# Como Instalar e rodar local:
**Atenção: Este passo requer a permição do dono ou integrantes de nível alto do projeto, ou não será permitido certas ações, como por exemplo, push e merge** 

$ git clone git@gitlab.com:GaidenX/Site-Lotus-Server.git

$ cd Site-Lotus-Server

$ node index.js

# Servidor:

Os servidores heroku que são hospedados o site é mantido por mim @GaidenX, por questão de segurança da aplicação, deploys e/ou mudança no servidor, entrar em contato.

# Teste:
Abrir o link a seguir para testar o site no ar:

[ Innyx Site, clique aqui! ](https://innix.herokuapp.com/)

# Design - Arte:

![](art/site-layout.jpg)