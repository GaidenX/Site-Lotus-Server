var fs = require("fs");
var express = require('express');
// force https on online page
var app = express().use(function (req, res, next) {
	if (req.header('x-forwarded-proto') == 'http') {
		res.redirect(301, 'https://' + 'innix.herokuapp.com' + req.url)
		return
	}
	next()
});
var server = require('https').createServer(app);
var porta = process.env.PORT || 3000;
var bodyParser = require('body-parser');
// var nodemailer = require('nodemailer'); //email for contact page

app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
	extended: true
}));
app.use(express.static(__dirname + '/public'));

//---

// HandleBars - Template Engine
var handleBars = require('express-handlebars');
var path = require('path');

app.set('views', path.join(__dirname, '/views'));
app.engine('handlebars', handleBars({
	defaultLayout: "index"
}));
app.set('view engine', 'handlebars');

//Pages Website
app.get('/home', function (request, response) {
	response.render('home', {
		bodyId: "home",
		style: ['//fonts.googleapis.com/css?family=Convergence', '/css/lightbox.min.css', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.7.3/tiny-slider.css'],
		javascript: ['https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.7.3/min/tiny-slider.js', '/js/lightbox.min.js', '/js/home.js']
	});
});

app.get('/about', function (request, response) {
	response.render('about', {
		bodyId: "about",
		style: [],
		javascript: []
	});
});

// Rotas de trocas de games na página
app.get('/homeconfig', function (req, res) {
	"use strict";
	var contents = fs.readFileSync("public/data/home.json");
	var jsonContent = JSON.parse(contents);
	res.send(jsonContent);
});

// app.post('/sendEmail', function (req, res) {
// 	var userEmail = req.body.email;
// 	var subjectEmail = req.body.subject;
// 	var messageEmail = req.body.message;

// 	var transporte = nodemailer.createTransport({
// 		service: 'Gmail',
// 		auth: {
// 			user: 'lotusgamesdev@gmail.com', // usuário
// 			pass: 'kakaio13' // senha da conta
// 		}
// 	});

// 	var email = {
// 		to: 'lotusgamesdev@gmail.com',
// 		from: 'lotusgamesdev@gmail.com', // Nosso email que sempre receberá
// 		subject: '' + subjectEmail + '', // titulo
// 		html: 'E-mail foi enviado por: ' + userEmail + ' .' + messageEmail + ''
// 	};

// 	transporte.sendMail(email, function (err, info) {
// 		if (err) {
// 			console.log('Oops, algo de errado aconteceu.');
// 		} else
// 			console.log('Email enviado!');
// 	});
// });

//The 404 Route
app.get('*', function (request, response) {
	response.render('home', {
		bodyId: "home",
		style: ['//fonts.googleapis.com/css?family=Convergence', '/css/lightbox.min.css', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.7.3/tiny-slider.css'],
		javascript: ['https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.7.3/min/tiny-slider.js', '/js/lightbox.min.js', '/js/home.js']
	});
});

app.listen(porta, function () {
	console.log("server on in: " + porta)
});